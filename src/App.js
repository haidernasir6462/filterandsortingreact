
import React, { useState, useRef, useEffect } from "react";
import Isotope from "isotope-layout";
import Airtable from "airtable";
import "./App.css";
import Loader from "react-loader-spinner";
//year filters
const years = ["2015", "2016", "2017", "2018", "2019", "2020", "2021"];
//Type filters
const Types = ["Bookshelves", "Chairs", "Lighting", "Rugs"];

export default function App() {
  // init one ref to store the future isotope object
  const isotope = useRef();
  //hook for stored data
  const [furnitures, setFurnitures] = useState([]);
  //hooks for filters
  const [selectedYear, setSelectedYear] = useState("*");
  const [selectedType, setSelectedType] = useState("*");
  //loading
  const [loading, setLoading] = useState(true);

  //fetch data from Airtable
  var base = new Airtable({
    apiKey: process.env.REACT_APP_AIRTABLE_API_KEY,
  }).base(process.env.REACT_APP_AIRTABLE_API_BASE);
  const table = base(process.env.REACT_APP_AIRTABLE_TABLE_NAME);
  const fetchTable = async () => {
    try {
      const data = await table.select().firstPage();
      const formattedData = data.map((item, index) => ({
        id: index + 1,
        ...item.fields,
      }));
      setFurnitures(formattedData);
      console.log("formattedData", formattedData);
      setLoading(false);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    fetchTable();
    //eslint-disable-next-line
  }, []);

  // initialize an Isotope object with configs
  useEffect(() => {
    isotope.current = new Isotope(".grid", {
      itemSelector: ".element-item",
      masonry: { gutter: 10, fitWidth: true },
    });
    // cleanup
    return () => isotope.current.destroy();
  }, [furnitures]);

  // handling filter key change
  useEffect(() => {
    if (selectedYear === "*" && selectedType === "*") {
      isotope.current.arrange({ filter: `*` });
    } else if (selectedYear !== "*" && selectedType === "*") {
      isotope.current.arrange({
        filter: `.year-${selectedYear}`,
      });
    } else if (selectedYear === "*" && selectedType !== "*") {
      isotope.current.arrange({
        filter: `.${selectedType}`,
      });
      console.log(`selectedType, .${selectedType}`);
    } else {
      isotope.current.arrange({
        filter: `.year-${selectedYear}.${selectedType}`,
      });
    }
  }, [selectedYear, selectedType]);

  return (
    <>
      <div className="filters">
        <div style={{ backgroundColor: "#a7000031", padding: "10px 0 0 0" }}>
          <div className="group1">
            <div id="years" className="button-group">
              <div
                style={{
                  fontSize: "16px",
                  padding: "4px 10px",
                  float: "left",
                }}
              >
                <b>YEAR |</b>
              </div>
              <button
                className={`button ${selectedYear === "*" && "is-checked"}`}
                onClick={() => {
                  setSelectedYear("*");
                }}
              >
                ALL
              </button>
              {years.map((year) => {
                return (
                  <button
                    key={year}
                    className={`button ${
                      selectedYear === year && "is-checked"
                    }`}
                    onClick={() => {
                      setSelectedYear(year);
                    }}
                  >
                    {year}
                  </button>
                );
              })}
            </div>
          </div>

          <div className="group2">
            <div className="button-group">
              <div
                style={{ fontSize: "16px", padding: "4px 12px", float: "left" }}
              >
                <b>TYPE |</b>
              </div>
              <button
                className={`button ${selectedType === "*" && "is-checked"}`}
                onClick={() => {
                  setSelectedType("*");
                }}
              >
                ALL
              </button>
              {Types.map((type) => {
                return (
                  <button
                    key={type}
                    className={`button ${
                      selectedType === type && "is-checked"
                    }`}
                    onClick={() => {
                      setSelectedType(type);
                    }}
                  >
                    {type}
                  </button>
                );
              })}
            </div>
          </div>
        </div>
      </div>

      {loading && (
        <div
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            width: "100px",
            marginTop: "30vh",
          }}
        >
          <Loader
            type="TailSpin"
            color="#a7000031"
            height={100}
            width={100}
            timeout={3000} //3 secs
          />
        </div>
      )}

      <div className="grid">
        {furnitures.length > 0 &&
          furnitures.map((furniture) => {
            console.log(
              `element-item year-${furniture.year} ${furniture.Type}`
            );
            return (
              <div
                key={furniture.id}
                className={`element-item year-${furniture.year} ${furniture.Type}`}
              >
                <img src={furniture.Images && furniture.Images[0].url} alt="" />
                <figcaption>{furniture.Name}</figcaption>
              </div>
            );
          })}
      </div>
    </>
  );
}
